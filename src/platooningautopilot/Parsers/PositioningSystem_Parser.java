/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Parsers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import platooningautopilot.Models.*;
import java.io.FileReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author anas_
 */
public class PositioningSystem_Parser extends FileParser<PositioningSystem> {
    private boolean isResampled;

    public PositioningSystem_Parser() {
        super();
    }

    public PositioningSystem_Parser(boolean isResampled) {
        super();
        this.isResampled = isResampled;
    }

    public List<PositioningSystem> parseFile(String fileName) {
        List<PositioningSystem> toRet = new ArrayList<PositioningSystem>();
        try {
            URI uri = new URI(fileName);
            fileName = uri.getPath();

            Scanner in = new Scanner(new FileReader(fileName));
            // sets the delimiter pattern
            in.useDelimiter("\n");

            if (in.hasNext()) {
                String row = in.next();
                String[] columns = row.split(",");

                if (columns.length > 0) {
                    while (in.hasNext()) {
                        String[] data = in.next().split(",", columns.length);
                        int idx = 0;
                        PositioningSystem obj = new PositioningSystem();
                        for (String val : data) {
                            if (val.length() > 0) {
                                switch (idx) {
                                    case 0:
                                        obj.setLogTimeStamp(Long.parseLong(val));
                                        break;
                                    case 1:
                                        obj.setLogStationId(Integer.parseInt(val));
                                        break;
                                    case 2:
                                        obj.setLogApplicationId(Integer.parseInt(val));
                                        break;
                                    case 3:
                                        obj.setSpeed(Double.parseDouble(val));
                                        break;
                                    case 4:
                                        obj.setLongitude(Double.parseDouble(val));
                                        break;
                                    case 5:
                                        obj.setLatitude(Double.parseDouble(val));
                                        break;
                                    case 6:
                                        obj.setHeading(Double.parseDouble(val));
                                        break;
                                }
                            }
                            idx++;
                            if (isResampled && idx == 2) {
                                // logApplicationId is not in resampled data file
                                idx++;
                            }
                        }
                        toRet.add(obj);
                    }
                }
            }
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return toRet;
    }
}
