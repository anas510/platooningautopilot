/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Parsers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import platooningautopilot.Models.*;
import java.io.FileReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author anas_
 */
public class VehicleDynamics_Parser extends FileParser<VehicleDynamics> {
    public List<VehicleDynamics> parseFile(String fileName) {
        List<VehicleDynamics> toRet = new ArrayList<VehicleDynamics>();
        try {
            URI uri = new URI(fileName);
            fileName = uri.getPath();

            Scanner in = new Scanner(new FileReader(fileName));
            // sets the delimiter pattern
            in.useDelimiter("\n");

            if (in.hasNext()) {
                String row = in.next();
                String[] columns = row.split(",");

                if (columns.length > 0) {
                    while (in.hasNext()) {
                        String[] data = in.next().split(",", columns.length);
                        int idx = 0;
                        VehicleDynamics obj = new VehicleDynamics();
                        for (String val : data) {
                            if (val.length() > 0) {
                                switch (idx) {
                                    case 0:
                                        obj.setLogTimeStamp(Long.parseLong(val));
                                        break;
                                    case 1:
                                        obj.setLogStationId(Integer.parseInt(val));
                                        break;
                                    case 2:
                                        obj.setLogApplicationId(Integer.parseInt(val));
                                        break;
                                    case 3:
                                        obj.setYaWrate(Double.parseDouble(val));
                                        break;
                                    case 4:
                                        obj.setAccLateral(Double.parseDouble(val));
                                        break;
                                    case 5:
                                        obj.setAccLongitudinal(Double.parseDouble(val));
                                        break;
                                    case 6:
                                        obj.setAccVertical(Double.parseDouble(val));
                                        break;
                                    case 7:
                                        obj.setSpeedWheelUnitDistance(Double.parseDouble(val));
                                        break;
                                    case 8:
                                        obj.setLaneChange(Double.parseDouble(val));
                                        break;
                                    case 9:
                                        obj.setSpeedLimit(Double.parseDouble(val));
                                        break;
                                }
                            }
                            idx++;
                        }
                        toRet.add(obj);
                    }
                }
            }
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return toRet;
    }
}
