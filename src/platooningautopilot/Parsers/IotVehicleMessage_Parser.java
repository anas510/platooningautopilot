/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Parsers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import platooningautopilot.Models.*;
import java.io.FileReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author anas_
 */
public class IotVehicleMessage_Parser extends FileParser<IotVehicleMessage> {
    public List<IotVehicleMessage> parseFile(String fileName) {
        List<IotVehicleMessage> toRet = new ArrayList<IotVehicleMessage>();
        try {
            URI uri = new URI(fileName);
            fileName = uri.getPath();

            Scanner in = new Scanner(new FileReader(fileName));
            // sets the delimiter pattern
            in.useDelimiter("\n");

            if (in.hasNext()) {
                String row = in.next();
                String[] columns = row.split(",");

                if (columns.length > 0) {
                    while (in.hasNext()) {
                        String[] data = in.next().split(",", columns.length);
                        int idx = 0;
                        IotVehicleMessage obj = new IotVehicleMessage();
                        for (String val : data) {
                            if (val.length() > 0) {
                                switch (idx) {
                                    case 0:
                                        obj.setLogTimeStamp(Long.parseLong(val));
                                        break;
                                    case 1:
                                        obj.setLogStationId(Integer.parseInt(val));
                                        break;
                                    case 2:
                                        obj.setLogApplicationId(Integer.parseInt(val));
                                        break;
                                    case 3:
                                        obj.setLogAction(val);
                                        break;
                                    case 4:
                                        obj.setLogCommunicationProfile(val);
                                        break;
                                    case 5:
                                        obj.setLogMessageType(val);
                                        break;
                                    case 6:
                                        obj.setStationId(Integer.parseInt(val));
                                        break;
                                    case 7:
                                        obj.parseAndSetData(val);
                                        break;
                                }
                            }
                            idx++;
                        }
                        toRet.add(obj);
                    }
                }
            }
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return toRet;
    }
}
