/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Parsers;

import platooningautopilot.Models.*;
import java.io.FileReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author anas_
 */
public class EnvironmentSensorsRelative_Parser extends FileParser<EnvironmentSensorsRelative> {
    public List<EnvironmentSensorsRelative> parseFile(String fileName) {
        List<EnvironmentSensorsRelative> toRet = new ArrayList<EnvironmentSensorsRelative>();
        try {
            URI uri = new URI(fileName);
            fileName = uri.getPath();

            Scanner in = new Scanner(new FileReader(fileName));
            // sets the delimiter pattern
            in.useDelimiter("\n");

            if (in.hasNext()) {
                String row = in.next();
                String[] columns = row.split(",");

                if (columns.length > 0) {
                    while (in.hasNext()) {
                        String[] data = in.next().split(",", columns.length);
                        int idx = 0;
                        EnvironmentSensorsRelative obj = new EnvironmentSensorsRelative();
                        for (String val : data) {
                            if (val.length() > 0) {
                                switch (idx) {
                                    case 0:
                                        obj.setLogTimeStamp(Long.parseLong(val));
                                        break;
                                    case 1:
                                        obj.setLogStationId(Integer.parseInt(val));
                                        break;
                                    case 2:
                                        obj.setLogApplicationId(Integer.parseInt(val));
                                        break;
                                    case 3:
                                        obj.setX(Double.parseDouble(val));
                                        break;
                                    case 4:
                                        obj.setY(Double.parseDouble(val));
                                        break;
                                    case 5:
                                        obj.setObstacleId(Integer.parseInt(val));
                                        break;
                                    case 6:
                                        obj.setObstacleCovariance(Double.parseDouble(val));
                                        break;
                                    case 7:
                                        obj.setObjectClass(val);
                                        break;
                                    case 8:
                                        obj.setLaneWidthSensorBased(Double.parseDouble(val));
                                        break;
                                    case 9:
                                        obj.setLaneWidthMapBased(Double.parseDouble(val));
                                        break;
                                    case 10:
                                        obj.setTrafficSignDescription(val);
                                        break;
                                    case 11:
                                        obj.setSpeedLimitSign(Double.parseDouble(val));
                                        break;
                                    case 12:
                                        obj.setServiceCategory(val);
                                        break;
                                    case 13:
                                        obj.setServiceCategoryCode(Double.parseDouble(val));
                                        break;
                                    case 14:
                                        obj.setCountryCode(val);
                                        break;
                                    case 15:
                                        obj.setPictogramCategoryCode(val);
                                        break;
                                    case 16:
                                        obj.setVruPedestrianClass(val);
                                        break;
                                    case 17:
                                        obj.setVruCyclistClass(val);
                                        break;
                                    case 18:
                                        obj.setConfidenceLevels(Double.parseDouble(val));
                                        break;
                                    case 19:
                                        obj.setEnvironInfo(Double.parseDouble(val));
                                        break;
                                    case 20:
                                        obj.setRoadHazard(Double.parseDouble(val));
                                        break;
                                    case 21:
                                        obj.setSensorPosition(Double.parseDouble(val));
                                        break;
                                    case 22:
                                        obj.setProcessDelay(Double.parseDouble(val));
                                        break;
                                }
                            }
                            idx++;
                        }
                        toRet.add(obj);
                    }
                }
            }
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return toRet;
    }
}
