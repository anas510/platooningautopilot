/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Models;

/**
 *
 * @author anas_
 */
public class EnvironmentSensorsRelative {
    private long logTimeStamp;
    private int logStationId;
    private int logApplicationId;
    private double x;
    private double y;
    private int obstacleId;
    private double obstacleCovariance;
    private String objectClass;
    private double laneWidthSensorBased;
    private double laneWidthMapBased;
    private String trafficSignDescription;
    private double speedLimitSign;
    private String serviceCategory;
    private double serviceCategoryCode;
    private String countryCode;
    private String pictogramCategoryCode;
    private String vruPedestrianClass;
    private String vruCyclistClass;
    private double confidenceLevels;
    private double environInfo;
    private double roadHazard;
    private double sensorPosition;
    private double processDelay;

    /**
     * @return the logTimeStamp
     */
    public long getLogTimeStamp() {
        return logTimeStamp;
    }

    /**
     * @param logTimeStamp the logTimeStamp to set
     */
    public void setLogTimeStamp(long logTimeStamp) {
        this.logTimeStamp = logTimeStamp;
    }

    /**
     * @return the logStationId
     */
    public int getLogStationId() {
        return logStationId;
    }

    /**
     * @param logStationId the logStationId to set
     */
    public void setLogStationId(int logStationId) {
        this.logStationId = logStationId;
    }

    /**
     * @return the logApplicationId
     */
    public int getLogApplicationId() {
        return logApplicationId;
    }

    /**
     * @param logApplicationId the logApplicationId to set
     */
    public void setLogApplicationId(int logApplicationId) {
        this.logApplicationId = logApplicationId;
    }

    /**
     * @return the x
     */
    public double getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public double getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * @return the obstacleId
     */
    public int getObstacleId() {
        return obstacleId;
    }

    /**
     * @param obstacleId the obstacleId to set
     */
    public void setObstacleId(int obstacleId) {
        this.obstacleId = obstacleId;
    }

    /**
     * @return the obstacleCovariance
     */
    public double getObstacleCovariance() {
        return obstacleCovariance;
    }

    /**
     * @param obstacleCovariance the obstacleCovariance to set
     */
    public void setObstacleCovariance(double obstacleCovariance) {
        this.obstacleCovariance = obstacleCovariance;
    }

    /**
     * @return the objectClass
     */
    public String getObjectClass() {
        return objectClass;
    }

    /**
     * @param objectClass the objectClass to set
     */
    public void setObjectClass(String objectClass) {
        this.objectClass = objectClass;
    }

    /**
     * @return the laneWidthSensorBased
     */
    public double getLaneWidthSensorBased() {
        return laneWidthSensorBased;
    }

    /**
     * @param laneWidthSensorBased the laneWidthSensorBased to set
     */
    public void setLaneWidthSensorBased(double laneWidthSensorBased) {
        this.laneWidthSensorBased = laneWidthSensorBased;
    }

    /**
     * @return the laneWidthMapBased
     */
    public double getLaneWidthMapBased() {
        return laneWidthMapBased;
    }

    /**
     * @param laneWidthMapBased the laneWidthMapBased to set
     */
    public void setLaneWidthMapBased(double laneWidthMapBased) {
        this.laneWidthMapBased = laneWidthMapBased;
    }

    /**
     * @return the trafficSignDescription
     */
    public String getTrafficSignDescription() {
        return trafficSignDescription;
    }

    /**
     * @param trafficSignDescription the trafficSignDescription to set
     */
    public void setTrafficSignDescription(String trafficSignDescription) {
        this.trafficSignDescription = trafficSignDescription;
    }

    /**
     * @return the speedLimitSign
     */
    public double getSpeedLimitSign() {
        return speedLimitSign;
    }

    /**
     * @param speedLimitSign the speedLimitSign to set
     */
    public void setSpeedLimitSign(double speedLimitSign) {
        this.speedLimitSign = speedLimitSign;
    }

    /**
     * @return the serviceCategory
     */
    public String getServiceCategory() {
        return serviceCategory;
    }

    /**
     * @param serviceCategory the serviceCategory to set
     */
    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    /**
     * @return the serviceCategoryCode
     */
    public double getServiceCategoryCode() {
        return serviceCategoryCode;
    }

    /**
     * @param serviceCategoryCode the serviceCategoryCode to set
     */
    public void setServiceCategoryCode(double serviceCategoryCode) {
        this.serviceCategoryCode = serviceCategoryCode;
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return the pictogramCatergoryCode
     */
    public String getPictogramCategoryCode() {
        return pictogramCategoryCode;
    }

    /**
     * @param pictogramCategoryCode the pictogramCatergoryCode to set
     */
    public void setPictogramCategoryCode(String pictogramCategoryCode) {
        this.pictogramCategoryCode = pictogramCategoryCode;
    }

    /**
     * @return the vruPedestrianClass
     */
    public String getVruPedestrianClass() {
        return vruPedestrianClass;
    }

    /**
     * @param vruPedestrianClass the vruPedestrianClass to set
     */
    public void setVruPedestrianClass(String vruPedestrianClass) {
        this.vruPedestrianClass = vruPedestrianClass;
    }

    /**
     * @return the vruCyclistClass
     */
    public String getVruCyclistClass() {
        return vruCyclistClass;
    }

    /**
     * @param vruCyclistClass the vruCyclistClass to set
     */
    public void setVruCyclistClass(String vruCyclistClass) {
        this.vruCyclistClass = vruCyclistClass;
    }

    /**
     * @return the confidenceLevels
     */
    public double getConfidenceLevels() {
        return confidenceLevels;
    }

    /**
     * @param confidenceLevels the confidenceLevels to set
     */
    public void setConfidenceLevels(double confidenceLevels) {
        this.confidenceLevels = confidenceLevels;
    }

    /**
     * @return the environInfo
     */
    public double getEnvironInfo() {
        return environInfo;
    }

    /**
     * @param environInfo the environInfo to set
     */
    public void setEnvironInfo(double environInfo) {
        this.environInfo = environInfo;
    }

    /**
     * @return the roadHazard
     */
    public double getRoadHazard() {
        return roadHazard;
    }

    /**
     * @param roadHazard the roadHazard to set
     */
    public void setRoadHazard(double roadHazard) {
        this.roadHazard = roadHazard;
    }

    /**
     * @return the sensorPosition
     */
    public double getSensorPosition() {
        return sensorPosition;
    }

    /**
     * @param sensorPosition the sensorPosition to set
     */
    public void setSensorPosition(double sensorPosition) {
        this.sensorPosition = sensorPosition;
    }

    /**
     * @return the processDelay
     */
    public double getProcessDelay() {
        return processDelay;
    }

    /**
     * @param processDelay the processDelay to set
     */
    public void setProcessDelay(double processDelay) {
        this.processDelay = processDelay;
    }

}
