/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Models;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import platooningautopilot.Models.Common.PositionEstimate;
import platooningautopilot.Models.Common.VehicleMetaData;

/**
 *
 * @author anas_
 */
public class IotVehicleMessage {
    private long logTimeStamp;
    private int logStationId;
    private int logApplicationId;
    private String logAction;
    private String logCommunicationProfile;
    private String logMessageType;
    private int stationId;
    private IotVehicleMessageMain Data;

    /**
     * @return the logTimeStamp
     */
    public long getLogTimeStamp() {
        return logTimeStamp;
    }

    /**
     * @param logTimeStamp the logTimeStamp to set
     */
    public void setLogTimeStamp(long logTimeStamp) {
        this.logTimeStamp = logTimeStamp;
    }

    /**
     * @return the logStationId
     */
    public int getLogStationId() {
        return logStationId;
    }

    /**
     * @param logStationId the logStationId to set
     */
    public void setLogStationId(int logStationId) {
        this.logStationId = logStationId;
    }

    /**
     * @return the logApplicationId
     */
    public int getLogApplicationId() {
        return logApplicationId;
    }

    /**
     * @param logApplicationId the logApplicationId to set
     */
    public void setLogApplicationId(int logApplicationId) {
        this.logApplicationId = logApplicationId;
    }

    /**
     * @return the logAction
     */
    public String getLogAction() {
        return logAction;
    }

    /**
     * @param logAction the logAction to set
     */
    public void setLogAction(String logAction) {
        this.logAction = logAction;
    }

    /**
     * @return the logCommunicationProfile
     */
    public String getLogCommunicationProfile() {
        return logCommunicationProfile;
    }

    /**
     * @param logCommunicationProfile the logCommunicationProfile to set
     */
    public void setLogCommunicationProfile(String logCommunicationProfile) {
        this.logCommunicationProfile = logCommunicationProfile;
    }

    /**
     * @return the logMessageType
     */
    public String getLogMessageType() {
        return logMessageType;
    }

    /**
     * @param logMessageType the logMessageType to set
     */
    public void setLogMessageType(String logMessageType) {
        this.logMessageType = logMessageType;
    }

    /**
     * @return the stationId
     */
    public int getStationId() {
        return stationId;
    }

    /**
     * @param stationId the stationId to set
     */
    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    /**
     * @return the Data
     */
    public IotVehicleMessageMain getData() {
        return Data;
    }

    /**
     * @param Data the Data to set
     */
    public void setData(IotVehicleMessageMain Data) {
        this.Data = Data;
    }

    public void parseAndSetData(String val) {
        IotVehicleMessageMain parsedData = null;
        if (val != null && val.length() > 10) {
            Gson gson = new GsonBuilder()
                    .create();

            parsedData = gson.fromJson(val, IotVehicleMessageMain.class);
        }
        setData(parsedData);
    }
}

class IotVehicleMessageMain {
    @SerializedName("message")
    private IotVehicleMessageData message;

    /**
     * @return the message
     */
    public IotVehicleMessageData getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(IotVehicleMessageData message) {
        this.message = message;
    }
}

class IotVehicleMessageData {
    @SerializedName("path")
    private IotVehiclePath path;

    @SerializedName("envelope")
    private IotVehicleEnvelope envelope;

    /**
     * @return the path
     */
    public IotVehiclePath getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(IotVehiclePath path) {
        this.path = path;
    }

    /**
     * @return the envelope
     */
    public IotVehicleEnvelope getEnvelope() {
        return envelope;
    }

    /**
     * @param envelope the envelope to set
     */
    public void setEnvelope(IotVehicleEnvelope envelope) {
        this.envelope = envelope;
    }

}

class IotVehiclePath {
    @SerializedName("positionEstimate")
    private List<PositionEstimate> positionEstimate;

    /**
     * @return the positionEstimate
     */
    public List<PositionEstimate> getPositionEstimate() {
        return positionEstimate;
    }

    /**
     * @param positionEstimate the positionEstimate to set
     */
    public void setPositionEstimate(List<PositionEstimate> positionEstimate) {
        this.positionEstimate = positionEstimate;
    }

}

class IotVehicleEnvelope {
    @SerializedName("version")
    private float version;

    @SerializedName("vehicleMetaData")
    private VehicleMetaData vehicleMetaData;

    @SerializedName("submitter")
    private String submitter;

    @SerializedName("vehicleProfileID")
    private int vehicleProfileID;

    @SerializedName("transientVehicleID")
    private int transientVehicleID;

    @SerializedName("generated_TimeStampUTC_ms")
    private long generated_TimeStampUTC_ms;
}
