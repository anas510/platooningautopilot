/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Models;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import platooningautopilot.Models.Common.PositionEstimate;
import platooningautopilot.Models.Common.VehicleMetaData;

/**
 *
 * @author anas_
 */
public class VehicleDynamics {
    private long logTimeStamp;
    private int logStationId;
    private int logApplicationId;
    private double yaWrate;
    private double accLateral;
    private double accLongitudinal;
    private double accVertical;
    private double speedWheelUnitDistance;
    private double laneChange;
    private double speedLimit;

    /**
     * @return the logTimeStamp
     */
    public long getLogTimeStamp() {
        return logTimeStamp;
    }

    /**
     * @param logTimeStamp the logTimeStamp to set
     */
    public void setLogTimeStamp(long logTimeStamp) {
        this.logTimeStamp = logTimeStamp;
    }

    /**
     * @return the logStationId
     */
    public int getLogStationId() {
        return logStationId;
    }

    /**
     * @param logStationId the logStationId to set
     */
    public void setLogStationId(int logStationId) {
        this.logStationId = logStationId;
    }

    /**
     * @return the logApplicationId
     */
    public int getLogApplicationId() {
        return logApplicationId;
    }

    /**
     * @param logApplicationId the logApplicationId to set
     */
    public void setLogApplicationId(int logApplicationId) {
        this.logApplicationId = logApplicationId;
    }

    /**
     * @return the yaWrate
     */
    public double getYaWrate() {
        return yaWrate;
    }

    /**
     * @param yaWrate the yaWrate to set
     */
    public void setYaWrate(double yaWrate) {
        this.yaWrate = yaWrate;
    }

    /**
     * @return the accLateral
     */
    public double getAccLateral() {
        return accLateral;
    }

    /**
     * @param accLateral the accLateral to set
     */
    public void setAccLateral(double accLateral) {
        this.accLateral = accLateral;
    }

    /**
     * @return the accLongitudinal
     */
    public double getAccLongitudinal() {
        return accLongitudinal;
    }

    /**
     * @param accLongitudinal the accLongitudinal to set
     */
    public void setAccLongitudinal(double accLongitudinal) {
        this.accLongitudinal = accLongitudinal;
    }

    /**
     * @return the accVertical
     */
    public double getAccVertical() {
        return accVertical;
    }

    /**
     * @param accVertical the accVertical to set
     */
    public void setAccVertical(double accVertical) {
        this.accVertical = accVertical;
    }

    /**
     * @return the speedWheelUnitDistance
     */
    public double getSpeedWheelUnitDistance() {
        return speedWheelUnitDistance;
    }

    /**
     * @param speedWheelUnitDistance the speedWheelUnitDistance to set
     */
    public void setSpeedWheelUnitDistance(double speedWheelUnitDistance) {
        this.speedWheelUnitDistance = speedWheelUnitDistance;
    }

    /**
     * @return the laneChange
     */
    public double getLaneChange() {
        return laneChange;
    }

    /**
     * @param laneChange the laneChange to set
     */
    public void setLaneChange(double laneChange) {
        this.laneChange = laneChange;
    }

    /**
     * @return the speedLimit
     */
    public double getSpeedLimit() {
        return speedLimit;
    }

    /**
     * @param speedLimit the speedLimit to set
     */
    public void setSpeedLimit(double speedLimit) {
        this.speedLimit = speedLimit;
    }

}
