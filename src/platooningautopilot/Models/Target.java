/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Models;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import platooningautopilot.Models.Common.PositionEstimate;
import platooningautopilot.Models.Common.VehicleMetaData;

/**
 *
 * @author anas_
 */
public class Target {
    private double logTimeStamp;
    private int logStationId;
    private int logApplicationId;
    private int targetId;
    private double x;
    private double y;
    private double theta;
    private int dataMode;
    private double actualTimeGap;
    private int desiredTimeGap;
    private double actualDistance;

    /**
     * @return the logTimeStamp
     */
    public double getLogTimeStamp() {
        return logTimeStamp;
    }

    /**
     * @param logTimeStamp the logTimeStamp to set
     */
    public void setLogTimeStamp(double logTimeStamp) {
        this.logTimeStamp = logTimeStamp;
    }

    /**
     * @return the logStationId
     */
    public int getLogStationId() {
        return logStationId;
    }

    /**
     * @param logStationId the logStationId to set
     */
    public void setLogStationId(int logStationId) {
        this.logStationId = logStationId;
    }

    /**
     * @return the logApplicationId
     */
    public int getLogApplicationId() {
        return logApplicationId;
    }

    /**
     * @param logApplicationId the logApplicationId to set
     */
    public void setLogApplicationId(int logApplicationId) {
        this.logApplicationId = logApplicationId;
    }

    /**
     * @return the targetId
     */
    public int getTargetId() {
        return targetId;
    }

    /**
     * @param targetId the targetId to set
     */
    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    /**
     * @return the x
     */
    public double getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public double getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * @return the theta
     */
    public double getTheta() {
        return theta;
    }

    /**
     * @param theta the theta to set
     */
    public void setTheta(double theta) {
        this.theta = theta;
    }

    /**
     * @return the dataMode
     */
    public int getDataMode() {
        return dataMode;
    }

    /**
     * @param dataMode the dataMode to set
     */
    public void setDataMode(int dataMode) {
        this.dataMode = dataMode;
    }

    /**
     * @return the actualTimeGap
     */
    public double getActualTimeGap() {
        return actualTimeGap;
    }

    /**
     * @param actualTimeGap the actualTimeGap to set
     */
    public void setActualTimeGap(double actualTimeGap) {
        this.actualTimeGap = actualTimeGap;
    }

    /**
     * @return the desiredTimeGap
     */
    public int getDesiredTimeGap() {
        return desiredTimeGap;
    }

    /**
     * @param desiredTimeGap the desiredTimeGap to set
     */
    public void setDesiredTimeGap(int desiredTimeGap) {
        this.desiredTimeGap = desiredTimeGap;
    }

    /**
     * @return the actualDistance
     */
    public double getActualDistance() {
        return actualDistance;
    }

    /**
     * @param actualDistance the actualDistance to set
     */
    public void setActualDistance(double actualDistance) {
        this.actualDistance = actualDistance;
    }
}
