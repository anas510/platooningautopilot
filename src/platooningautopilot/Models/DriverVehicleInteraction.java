/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Models;

/**
 *
 * @author anas_
 */
public class DriverVehicleInteraction {

    private long logTimeStamp;
    private int logStationId;
    private int logApplicationId;
    private double throttleStatus;
    private double clutchStatus;
    private double breakStatus;
    private double wiperStatus;
    private double steeringWheel;
    private double breakForce;
    /**
     * @return the logTimeStamp
     */
    public long getLogTimeStamp() {
        return logTimeStamp;
    }

    /**
     * @param logTimeStamp the logTimeStamp to set
     */
    public void setLogTimeStamp(long logTimeStamp) {
        this.logTimeStamp = logTimeStamp;
    }

    /**
     * @return the logStationId
     */
    public int getLogStationId() {
        return logStationId;
    }

    /**
     * @param logStationId the logStationId to set
     */
    public void setLogStationId(int logStationId) {
        this.logStationId = logStationId;
    }

    /**
     * @return the logApplicationId
     */
    public int getApplicationId() {
        return logApplicationId;
    }

    /**
     * @param applicationId the logApplicationId to set
     */
    public void setApplicationId(int applicationId) {
        this.logApplicationId = applicationId;
    }

    /**
     * @return the throttleStatus
     */
    public double getThrottleStatus() {
        return throttleStatus;
    }

    /**
     * @param throttleStatus the throttleStatus to set
     */
    public void setThrottleStatus(double throttleStatus) {
        this.throttleStatus = throttleStatus;
    }

    /**
     * @return the clutchStatus
     */
    public double getClutchStatus() {
        return clutchStatus;
    }

    /**
     * @param clutchStatus the clutchStatus to set
     */
    public void setClutchStatus(double clutchStatus) {
        this.clutchStatus = clutchStatus;
    }

    /**
     * @return the breakStatus
     */
    public double getBreakStatus() {
        return breakStatus;
    }

    /**
     * @param breakStatus the breakStatus to set
     */
    public void setBreakStatus(double breakStatus) {
        this.breakStatus = breakStatus;
    }

    /**
     * @return the wiperStatus
     */
    public double getWiperStatus() {
        return wiperStatus;
    }

    /**
     * @param wiperStatus the wiperStatus to set
     */
    public void setWiperStatus(double wiperStatus) {
        this.wiperStatus = wiperStatus;
    }

    /**
     * @return the steeringWheel
     */
    public double getSteeringWheel() {
        return steeringWheel;
    }

    /**
     * @param steeringWheel the steeringWheel to set
     */
    public void setSteeringWheel(double steeringWheel) {
        this.steeringWheel = steeringWheel;
    }

    /**
     * @return the breakForce
     */
    public double getBreakForce() {
        return breakForce;
    }

    /**
     * @param breakForce the breakForce to set
     */
    public void setBreakForce(double breakForce) {
        this.breakForce = breakForce;
    }
    
}
