/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Models;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import platooningautopilot.Models.Common.PositionEstimate;
import platooningautopilot.Models.Common.VehicleMetaData;

/**
 *
 * @author anas_
 */
public class PlatoonFormation {
    private long logTimeStamp;
    private int logStationId;
    private int logApplicationId;
    private String logAction;
    private String logCommunicationProfile;
    private int stationId;
    private PlatoonFormationData Data;

    /**
     * @return the logTimeStamp
     */
    public long getLogTimeStamp() {
        return logTimeStamp;
    }

    /**
     * @param logTimeStamp the logTimeStamp to set
     */
    public void setLogTimeStamp(long logTimeStamp) {
        this.logTimeStamp = logTimeStamp;
    }

    /**
     * @return the logStationId
     */
    public int getLogStationId() {
        return logStationId;
    }

    /**
     * @param logStationId the logStationId to set
     */
    public void setLogStationId(int logStationId) {
        this.logStationId = logStationId;
    }

    /**
     * @return the logApplicationId
     */
    public int getLogApplicationId() {
        return logApplicationId;
    }

    /**
     * @param logApplicationId the logApplicationId to set
     */
    public void setLogApplicationId(int logApplicationId) {
        this.logApplicationId = logApplicationId;
    }

    /**
     * @return the logAction
     */
    public String getLogAction() {
        return logAction;
    }

    /**
     * @param logAction the logAction to set
     */
    public void setLogAction(String logAction) {
        this.logAction = logAction;
    }

    /**
     * @return the logCommunicationProfile
     */
    public String getLogCommunicationProfile() {
        return logCommunicationProfile;
    }

    /**
     * @param logCommunicationProfile the logCommunicationProfile to set
     */
    public void setLogCommunicationProfile(String logCommunicationProfile) {
        this.logCommunicationProfile = logCommunicationProfile;
    }

    /**
     * @return the stationId
     */
    public int getStationId() {
        return stationId;
    }

    /**
     * @param stationId the stationId to set
     */
    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    /**
     * @return the Data
     */
    public PlatoonFormationData getData() {
        return Data;
    }

    /**
     * @param Data the Data to set
     */
    public void setData(PlatoonFormationData Data) {
        this.Data = Data;
    }

    public void parseAndSetData(String val) {
        PlatoonFormationData parsedData = null;
        if (val != null && val.length() > 10) {
            Gson gson = new GsonBuilder()
                    .create();

            parsedData = gson.fromJson(val, PlatoonFormationData.class);
        }
        setData(parsedData);
    }
}

class PlatoonFormationData {
    @SerializedName("ETA")
    private double ETA;

    @SerializedName("messageID")
    private int messageId;

    @SerializedName("platoonID")
    private int platoonId;

    @SerializedName("stationID")
    private int stationId;

    @SerializedName("handOverToPS")
    private boolean handOverToPS;

    @SerializedName("generationTimestampUTC")
    private long generationTimestampUTC;

    @SerializedName("rendevouzPointLatitude")
    private double rendevouzPointLatitude;

    @SerializedName("rendevouzPointLongitude")
    private double rendevouzPointLongitude;

    @SerializedName("plannerSpeedAdvice")
    private double[] plannerSpeedAdvice;

    @SerializedName("plannerTimeAdviceSecs")
    private double[] plannerTimeAdviceSecs;

    @SerializedName("plannerTimeAdviceNsecs")
    private double[] plannerTimeAdviceNsecs;

    @SerializedName("plannerPositionAdviceLat")
    private double[] plannerPositionAdviceLat;

    @SerializedName("plannerPositionAdviceLong")
    private double[] plannerPositionAdviceLong;

    /**
     * @return the ETA
     */
    public double getETA() {
        return ETA;
    }

    /**
     * @param ETA the ETA to set
     */
    public void setETA(double ETA) {
        this.ETA = ETA;
    }

    /**
     * @return the messageId
     */
    public int getMessageId() {
        return messageId;
    }

    /**
     * @param messageId the messageId to set
     */
    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    /**
     * @return the platoonId
     */
    public int getPlatoonId() {
        return platoonId;
    }

    /**
     * @param platoonId the platoonId to set
     */
    public void setPlatoonId(int platoonId) {
        this.platoonId = platoonId;
    }

    /**
     * @return the stationId
     */
    public int getStationId() {
        return stationId;
    }

    /**
     * @param stationId the stationId to set
     */
    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    /**
     * @return the handOverToPS
     */
    public boolean isHandOverToPS() {
        return handOverToPS;
    }

    /**
     * @param handOverToPS the handOverToPS to set
     */
    public void setHandOverToPS(boolean handOverToPS) {
        this.handOverToPS = handOverToPS;
    }

    /**
     * @return the generationTimestampUTC
     */
    public long getGenerationTimestampUTC() {
        return generationTimestampUTC;
    }

    /**
     * @param generationTimestampUTC the generationTimestampUTC to set
     */
    public void setGenerationTimestampUTC(long generationTimestampUTC) {
        this.generationTimestampUTC = generationTimestampUTC;
    }

    /**
     * @return the rendevouzPointLatitude
     */
    public double getRendevouzPointLatitude() {
        return rendevouzPointLatitude;
    }

    /**
     * @param rendevouzPointLatitude the rendevouzPointLatitude to set
     */
    public void setRendevouzPointLatitude(double rendevouzPointLatitude) {
        this.rendevouzPointLatitude = rendevouzPointLatitude;
    }

    /**
     * @return the rendevouzPointLongitude
     */
    public double getRendevouzPointLongitude() {
        return rendevouzPointLongitude;
    }

    /**
     * @param rendevouzPointLongitude the rendevouzPointLongitude to set
     */
    public void setRendevouzPointLongitude(double rendevouzPointLongitude) {
        this.rendevouzPointLongitude = rendevouzPointLongitude;
    }

    /**
     * @return the plannerSpeedAdvice
     */
    public double[] getPlannerSpeedAdvice() {
        return plannerSpeedAdvice;
    }

    /**
     * @param plannerSpeedAdvice the plannerSpeedAdvice to set
     */
    public void setPlannerSpeedAdvice(double[] plannerSpeedAdvice) {
        this.plannerSpeedAdvice = plannerSpeedAdvice;
    }

    /**
     * @return the plannerTimeAdviceSecs
     */
    public double[] getPlannerTimeAdviceSecs() {
        return plannerTimeAdviceSecs;
    }

    /**
     * @param plannerTimeAdviceSecs the plannerTimeAdviceSecs to set
     */
    public void setPlannerTimeAdviceSecs(double[] plannerTimeAdviceSecs) {
        this.plannerTimeAdviceSecs = plannerTimeAdviceSecs;
    }

    /**
     * @return the plannerTimeAdviceNsecs
     */
    public double[] getPlannerTimeAdviceNsecs() {
        return plannerTimeAdviceNsecs;
    }

    /**
     * @param plannerTimeAdviceNsecs the plannerTimeAdviceNsecs to set
     */
    public void setPlannerTimeAdviceNsecs(double[] plannerTimeAdviceNsecs) {
        this.plannerTimeAdviceNsecs = plannerTimeAdviceNsecs;
    }

    /**
     * @return the plannerPositionAdviceLat
     */
    public double[] getPlannerPositionAdviceLat() {
        return plannerPositionAdviceLat;
    }

    /**
     * @param plannerPositionAdviceLat the plannerPositionAdviceLat to set
     */
    public void setPlannerPositionAdviceLat(double[] plannerPositionAdviceLat) {
        this.plannerPositionAdviceLat = plannerPositionAdviceLat;
    }

    /**
     * @return the plannerPositionAdviceLong
     */
    public double[] getPlannerPositionAdviceLong() {
        return plannerPositionAdviceLong;
    }

    /**
     * @param plannerPositionAdviceLong the plannerPositionAdviceLong to set
     */
    public void setPlannerPositionAdviceLong(double[] plannerPositionAdviceLong) {
        this.plannerPositionAdviceLong = plannerPositionAdviceLong;
    }

}
