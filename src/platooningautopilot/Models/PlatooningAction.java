/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Models;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import platooningautopilot.Models.Common.PositionEstimate;
import platooningautopilot.Models.Common.VehicleMetaData;

/**
 *
 * @author anas_
 */
public class PlatooningAction {
    private long logTimeStamp;
    private int logStationId;
    private int logApplicationId;
    private int eventId;
    private int eventModelId;
    private int eventActionId;
    private int platooningServiceId;
    private int leaderStationId;
    private int platoonId;
    private long generationTimestampUTC;

    /**
     * @return the logTimeStamp
     */
    public long getLogTimeStamp() {
        return logTimeStamp;
    }

    /**
     * @param logTimeStamp the logTimeStamp to set
     */
    public void setLogTimeStamp(long logTimeStamp) {
        this.logTimeStamp = logTimeStamp;
    }

    /**
     * @return the logStationId
     */
    public int getLogStationId() {
        return logStationId;
    }

    /**
     * @param logStationId the logStationId to set
     */
    public void setLogStationId(int logStationId) {
        this.logStationId = logStationId;
    }

    /**
     * @return the logApplicationId
     */
    public int getLogApplicationId() {
        return logApplicationId;
    }

    /**
     * @param logApplicationId the logApplicationId to set
     */
    public void setLogApplicationId(int logApplicationId) {
        this.logApplicationId = logApplicationId;
    }

    /**
     * @return the eventId
     */
    public int getEventId() {
        return eventId;
    }

    /**
     * @param eventId the eventId to set
     */
    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    /**
     * @return the eventModelId
     */
    public int getEventModelId() {
        return eventModelId;
    }

    /**
     * @param eventModelId the eventModelId to set
     */
    public void setEventModelId(int eventModelId) {
        this.eventModelId = eventModelId;
    }

    /**
     * @return the eventActionId
     */
    public int getEventActionId() {
        return eventActionId;
    }

    /**
     * @param eventActionId the eventActionId to set
     */
    public void setEventActionId(int eventActionId) {
        this.eventActionId = eventActionId;
    }

    /**
     * @return the platooningServiceId
     */
    public int getPlatooningServiceId() {
        return platooningServiceId;
    }

    /**
     * @param platooningServiceId the platooningServiceId to set
     */
    public void setPlatooningServiceId(int platooningServiceId) {
        this.platooningServiceId = platooningServiceId;
    }

    /**
     * @return the leaderStationId
     */
    public int getLeaderStationId() {
        return leaderStationId;
    }

    /**
     * @param leaderStationId the leaderStationId to set
     */
    public void setLeaderStationId(int leaderStationId) {
        this.leaderStationId = leaderStationId;
    }

    /**
     * @return the platoonId
     */
    public int getPlatoonId() {
        return platoonId;
    }

    /**
     * @param platoonId the platoonId to set
     */
    public void setPlatoonId(int platoonId) {
        this.platoonId = platoonId;
    }

    /**
     * @return the generationTimestampUTC
     */
    public long getGenerationTimestampUTC() {
        return generationTimestampUTC;
    }

    /**
     * @param generationTimestampUTC the generationTimestampUTC to set
     */
    public void setGenerationTimestampUTC(long generationTimestampUTC) {
        this.generationTimestampUTC = generationTimestampUTC;
    }

}
