/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Models;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import platooningautopilot.Models.Common.PositionEstimate;
import platooningautopilot.Models.Common.VehicleMetaData;

/**
 *
 * @author anas_
 */
public class PlatoonStatus {
    private long logTimeStamp;
    private int logStationId;
    private int logApplicationId;
    private String logAction;
    private String logCommunicationProfile;
    private int stationId;
    private PlatoonStatusData Data;

    /**
     * @return the logTimeStamp
     */
    public long getLogTimeStamp() {
        return logTimeStamp;
    }

    /**
     * @param logTimeStamp the logTimeStamp to set
     */
    public void setLogTimeStamp(long logTimeStamp) {
        this.logTimeStamp = logTimeStamp;
    }

    /**
     * @return the logStationId
     */
    public int getLogStationId() {
        return logStationId;
    }

    /**
     * @param logStationId the logStationId to set
     */
    public void setLogStationId(int logStationId) {
        this.logStationId = logStationId;
    }

    /**
     * @return the logApplicationId
     */
    public int getLogApplicationId() {
        return logApplicationId;
    }

    /**
     * @param logApplicationId the logApplicationId to set
     */
    public void setLogApplicationId(int logApplicationId) {
        this.logApplicationId = logApplicationId;
    }

    /**
     * @return the logAction
     */
    public String getLogAction() {
        return logAction;
    }

    /**
     * @param logAction the logAction to set
     */
    public void setLogAction(String logAction) {
        this.logAction = logAction;
    }

    /**
     * @return the logCommunicationProfile
     */
    public String getLogCommunicationProfile() {
        return logCommunicationProfile;
    }

    /**
     * @param logCommunicationProfile the logCommunicationProfile to set
     */
    public void setLogCommunicationProfile(String logCommunicationProfile) {
        this.logCommunicationProfile = logCommunicationProfile;
    }

    /**
     * @return the stationId
     */
    public int getStationId() {
        return stationId;
    }

    /**
     * @param stationId the stationId to set
     */
    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    /**
     * @return the Data
     */
    public PlatoonStatusData getData() {
        return Data;
    }

    /**
     * @param Data the Data to set
     */
    public void setData(PlatoonStatusData Data) {
        this.Data = Data;
    }

    public void parseAndSetData(String val) {
        PlatoonStatusData parsedData = null;
        if (val != null && val.length() > 10) {
            Gson gson = new GsonBuilder()
                    .create();

            parsedData = gson.fromJson(val, PlatoonStatusData.class);
        }
        setData(parsedData);
    }

}

class PlatoonStatusData {
    @SerializedName("messageID")
    private int messageId;

    @SerializedName("platoonID")
    private int platoonId;

    @SerializedName("stationID")
    private int stationId;

    @SerializedName("platoonSize")
    private int platoonSize;

    @SerializedName("vehicleMode")
    private int vehicleMode;

    @SerializedName("vehicleRole")
    private int vehicleRole;

    @SerializedName("platoonState")
    private int platoonState;

    @SerializedName("platoonDestination")
    private String platoonDestination;

    @SerializedName("generationTimestampUTC")
    private long generationTimestampUTC;

    /**
     * @return the messageId
     */
    public int getMessageId() {
        return messageId;
    }

    /**
     * @param messageId the messageId to set
     */
    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    /**
     * @return the platoonId
     */
    public int getPlatoonId() {
        return platoonId;
    }

    /**
     * @param platoonId the platoonId to set
     */
    public void setPlatoonId(int platoonId) {
        this.platoonId = platoonId;
    }

    /**
     * @return the stationId
     */
    public int getStationId() {
        return stationId;
    }

    /**
     * @param stationId the stationId to set
     */
    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    /**
     * @return the platoonSize
     */
    public int getPlatoonSize() {
        return platoonSize;
    }

    /**
     * @param platoonSize the platoonSize to set
     */
    public void setPlatoonSize(int platoonSize) {
        this.platoonSize = platoonSize;
    }

    /**
     * @return the vehicleMode
     */
    public int getVehicleMode() {
        return vehicleMode;
    }

    /**
     * @param vehicleMode the vehicleMode to set
     */
    public void setVehicleMode(int vehicleMode) {
        this.vehicleMode = vehicleMode;
    }

    /**
     * @return the vehicleRole
     */
    public int getVehicleRole() {
        return vehicleRole;
    }

    /**
     * @param vehicleRole the vehicleRole to set
     */
    public void setVehicleRole(int vehicleRole) {
        this.vehicleRole = vehicleRole;
    }

    /**
     * @return the platoonState
     */
    public int getPlatoonState() {
        return platoonState;
    }

    /**
     * @param platoonState the platoonState to set
     */
    public void setPlatoonState(int platoonState) {
        this.platoonState = platoonState;
    }

    /**
     * @return the platoonDestination
     */
    public String getPlatoonDestination() {
        return platoonDestination;
    }

    /**
     * @param platoonDestination the platoonDestination to set
     */
    public void setPlatoonDestination(String platoonDestination) {
        this.platoonDestination = platoonDestination;
    }

    /**
     * @return the generationTimestampUTC
     */
    public long getGenerationTimestampUTC() {
        return generationTimestampUTC;
    }

    /**
     * @param generationTimestampUTC the generationTimestampUTC to set
     */
    public void setGenerationTimestampUTC(long generationTimestampUTC) {
        this.generationTimestampUTC = generationTimestampUTC;
    }

}
