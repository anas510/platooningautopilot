/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Models.Common;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author anas_
 */
public class PositionEstimate {

    @SerializedName("speed_mps")
    private double speed_MPS;

    @SerializedName("altitude_m")
    private double altitude_m;

    @SerializedName("heading_deg")
    private double headingDeg;

    @SerializedName("latitude_deg")
    private double latitudeDeg;

    @SerializedName("longitude_deg")
    private double longitudeDeg;

    @SerializedName("positionType")
    private String positionType;

    @SerializedName("timeStampUTC_ms")
    private long epochMS;

    @SerializedName("interpolatePoint")
    private boolean interpolatePoint;

    @SerializedName("speedAccuracy_mps")
    private double speedAccuracy_mps;

    @SerializedName("altitudeAccuracy_m")
    private double altitudeAccuracy_m;

    @SerializedName("speedDetectionType")
    private String speedDetectionType;

    @SerializedName("headingDetectionType")
    private String headingDetectionType;

    @SerializedName("headingAccuracy_deg")
    private double headingAccuracy_deg;

    @SerializedName("horizontalAccuracy_m")
    private double horizontalAccuracy_m;

    @SerializedName("currentLaneEstimation")
    private double currentLaneEstimation;

    /**
     * @return the speed_MPS
     */
    public double getSpeed_MPS() {
        return speed_MPS;
    }

    /**
     * @param speed_MPS the speed_MPS to set
     */
    public void setSpeed_MPS(double speed_MPS) {
        this.speed_MPS = speed_MPS;
    }

    /**
     * @return the altitude_m
     */
    public double getAltitude_m() {
        return altitude_m;
    }

    /**
     * @param altitude_m the altitude_m to set
     */
    public void setAltitude_m(double altitude_m) {
        this.altitude_m = altitude_m;
    }

    /**
     * @return the headingDeg
     */
    public double getHeadingDeg() {
        return headingDeg;
    }

    /**
     * @param headingDeg the headingDeg to set
     */
    public void setHeadingDeg(double headingDeg) {
        this.headingDeg = headingDeg;
    }

    /**
     * @return the latitudeDeg
     */
    public double getLatitudeDeg() {
        return latitudeDeg;
    }

    /**
     * @param latitudeDeg the latitudeDeg to set
     */
    public void setLatitudeDeg(double latitudeDeg) {
        this.latitudeDeg = latitudeDeg;
    }

    /**
     * @return the longitudeDeg
     */
    public double getLongitudeDeg() {
        return longitudeDeg;
    }

    /**
     * @param longitudeDeg the longitudeDeg to set
     */
    public void setLongitudeDeg(double longitudeDeg) {
        this.longitudeDeg = longitudeDeg;
    }

    /**
     * @return the positionType
     */
    public String getPositionType() {
        return positionType;
    }

    /**
     * @param positionType the positionType to set
     */
    public void setPositionType(String positionType) {
        this.positionType = positionType;
    }

    /**
     * @return the epochMS
     */
    public long getEpochMS() {
        return epochMS;
    }

    /**
     * @param epochMS the epochMS to set
     */
    public void setEpochMS(long epochMS) {
        this.epochMS = epochMS;
    }

    /**
     * @return the interpolatePoint
     */
    public boolean isInterpolatePoint() {
        return interpolatePoint;
    }

    /**
     * @param interpolatePoint the interpolatePoint to set
     */
    public void setInterpolatePoint(boolean interpolatePoint) {
        this.interpolatePoint = interpolatePoint;
    }

    /**
     * @return the speedAccuracy_mps
     */
    public double getSpeedAccuracy_mps() {
        return speedAccuracy_mps;
    }

    /**
     * @param speedAccuracy_mps the speedAccuracy_mps to set
     */
    public void setSpeedAccuracy_mps(double speedAccuracy_mps) {
        this.speedAccuracy_mps = speedAccuracy_mps;
    }

    /**
     * @return the altitudeAccuracy_m
     */
    public double getAltitudeAccuracy_m() {
        return altitudeAccuracy_m;
    }

    /**
     * @param altitudeAccuracy_m the altitudeAccuracy_m to set
     */
    public void setAltitudeAccuracy_m(double altitudeAccuracy_m) {
        this.altitudeAccuracy_m = altitudeAccuracy_m;
    }

    /**
     * @return the speedDetectionType
     */
    public String getSpeedDetectionType() {
        return speedDetectionType;
    }

    /**
     * @param speedDetectionType the speedDetectionType to set
     */
    public void setSpeedDetectionType(String speedDetectionType) {
        this.speedDetectionType = speedDetectionType;
    }

    /**
     * @return the headingDetectionType
     */
    public String getHeadingDetectionType() {
        return headingDetectionType;
    }

    /**
     * @param headingDetectionType the headingDetectionType to set
     */
    public void setHeadingDetectionType(String headingDetectionType) {
        this.headingDetectionType = headingDetectionType;
    }

    /**
     * @return the headingAccuracy_deg
     */
    public double getHeadingAccuracy_deg() {
        return headingAccuracy_deg;
    }

    /**
     * @param headingAccuracy_deg the headingAccuracy_deg to set
     */
    public void setHeadingAccuracy_deg(double headingAccuracy_deg) {
        this.headingAccuracy_deg = headingAccuracy_deg;
    }

    /**
     * @return the horizontalAccuracy_m
     */
    public double getHorizontalAccuracy_m() {
        return horizontalAccuracy_m;
    }

    /**
     * @param horizontalAccuracy_m the horizontalAccuracy_m to set
     */
    public void setHorizontalAccuracy_m(double horizontalAccuracy_m) {
        this.horizontalAccuracy_m = horizontalAccuracy_m;
    }

    /**
     * @return the currentLaneEstimation
     */
    public double getCurrentLaneEstimation() {
        return currentLaneEstimation;
    }

    /**
     * @param currentLaneEstimation the currentLaneEstimation to set
     */
    public void setCurrentLaneEstimation(double currentLaneEstimation) {
        this.currentLaneEstimation = currentLaneEstimation;
    }

}
