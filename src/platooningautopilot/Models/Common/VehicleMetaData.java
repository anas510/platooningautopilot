/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Models.Common;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author anas_
 */
public class VehicleMetaData {
    @SerializedName("vehicleWidth_m")
    private double vehicleWidth_m;

    @SerializedName("primaryFuelType")
    private String primaryFuelType;

    @SerializedName("secondaryFuelType")
    private String secondaryFuelType;

    @SerializedName("vehicleHeight_m")
    private double vehicleHeight_m;

    @SerializedName("vehicleLength_m")
    private double vehicleLength_m;

    @SerializedName("primaryFuelTankVolume")
    private double primaryFuelTankVolume;

    @SerializedName("secondaryFuelTankVolume")
    private double secondaryFuelTankVolume;

    @SerializedName("vehicleReferencePointDeltaAboveGround_m")
    private double vehicleReferencePointDeltaAboveGround_m;

    @SerializedName("vehicleTypeGenericEnum")
    private String vehicleTypeGenericEnum;

    @SerializedName("vehicleSpecificMetadata")
    private KeyValueObj vehicleSpecificMetadata;

}
