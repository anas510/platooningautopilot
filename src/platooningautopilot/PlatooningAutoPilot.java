/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot;

import java.util.List;
import platooningautopilot.Models.*;
import platooningautopilot.Helpers.Helpers;
import platooningautopilot.Parsers.*;

/**
 *
 * @author anas_
 */
public class PlatooningAutoPilot {

        /**
         * @param args the command line arguments
         */
        public static void main(String[] args) {

                // File 1 import
                DriverVehicleInteraction_Parser parser = new DriverVehicleInteraction_Parser();
                List<DriverVehicleInteraction> vehicleInteractions = parser.parseFile(
                                PlatooningAutoPilot.class.getResource("./data_files/DriverVehicleInteraction.csv")
                                                .getPath());
                System.out.println(vehicleInteractions.size() + " DriverVehicleInteractions records imported...");

                // File 2 import
                EnvironmentSensorsRelative_Parser parser2 = new EnvironmentSensorsRelative_Parser();
                List<EnvironmentSensorsRelative> envSensorsRelative = parser2.parseFile(
                                PlatooningAutoPilot.class.getResource("./data_files/EnvironmentSensorsRelative.csv")
                                                .getPath());
                System.out.println(envSensorsRelative.size() + " EnvironmentSensorsRelative records imported...");

                // File 3 import
                IotVehicleMessage_Parser parser3 = new IotVehicleMessage_Parser();
                List<IotVehicleMessage> vehicleMessages = parser3.parseFile(
                                PlatooningAutoPilot.class.getResource("./data_files/IotVehicleMessage.csv").getPath());
                System.out.println(vehicleMessages.size() + " IotVehicleMessage records imported...");

                // File 4 import
                PlatoonFormation_Parser parser4 = new PlatoonFormation_Parser();
                List<PlatoonFormation> platoonFormations = parser4.parseFile(
                                PlatooningAutoPilot.class.getResource("./data_files/PlatoonFormation.csv").getPath());
                System.out.println(platoonFormations.size() + " PlatoonFormations records imported...");

                // File 5 import
                PlatooningAction_Parser parser5 = new PlatooningAction_Parser();
                List<PlatooningAction> platooningActions = parser5.parseFile(
                                PlatooningAutoPilot.class.getResource("./data_files/PlatooningAction.csv").getPath());
                System.out.println(platooningActions.size() + " PlatooningActions records imported...");

                // File 6 import
                PlatooningEvent_Parser parser6 = new PlatooningEvent_Parser();
                List<PlatooningEvent> platooningEvents = parser6.parseFile(
                                PlatooningAutoPilot.class.getResource("./data_files/PlatooningEvent.csv").getPath());
                System.out.println(platooningEvents.size() + " PlatooningEvents records imported...");

                // File 7 import
                PlatoonStatus_Parser parser7 = new PlatoonStatus_Parser();
                List<PlatoonStatus> platoonStatuses = parser7.parseFile(
                                PlatooningAutoPilot.class.getResource("./data_files/PlatoonStatus.csv").getPath());
                System.out.println(platoonStatuses.size() + " PlatoonStatuses records imported...");

                // File 8 import
                PositioningSystem_Parser parser8 = new PositioningSystem_Parser();
                List<PositioningSystem> positioningSystems = parser8.parseFile(
                                PlatooningAutoPilot.class.getResource("./data_files/PositioningSystem.csv").getPath());
                System.out.println(positioningSystems.size() + " PositioningSystems records imported...");

                // File 9 import
                PositioningSystem_Parser parser9 = new PositioningSystem_Parser(true);
                List<PositioningSystem> positioningSystemResampled = parser9.parseFile(
                                PlatooningAutoPilot.class.getResource("./data_files/PositioningSystemResampled.csv")
                                                .getPath());
                System.out.println(
                                positioningSystemResampled.size() + " PositioningSystemResampled records imported...");

                // File 10 import
                VehicleDynamics_Parser parser10 = new VehicleDynamics_Parser();
                List<VehicleDynamics> vehicleDynamics = parser10.parseFile(
                                PlatooningAutoPilot.class.getResource("./data_files/VehicleDynamics.csv").getPath());
                System.out.println(vehicleDynamics.size() + " VehicleDynamics records imported...");

                // File 11 import
                TargetData_Parser parser11 = new TargetData_Parser();
                List<Target> targetRecords = parser11.parseFile(
                                PlatooningAutoPilot.class.getResource("./data_files/Target.csv").getPath());
                System.out.println(targetRecords.size() + " TargetRecords records imported...");
        }

}
