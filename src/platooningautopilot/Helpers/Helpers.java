/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package platooningautopilot.Helpers;

import com.sun.javafx.scene.control.skin.Utils;
import java.io.FileReader;
import java.net.URI;
import java.util.*;
import java.util.Scanner;

/**
 *
 * @author anas_
 */
public class Helpers {
    public static List<String> getColumnsList(String fileName) {
        List<String> toRet = new ArrayList<String>();
        try {
            URI uri = new URI(fileName);
            fileName=uri.getPath();
            
            Scanner in = new Scanner(new FileReader(fileName));
            // sets the delimiter pattern
            in.useDelimiter("\n");
            
            if (in.hasNext()) {
                String row = in.next();
                for(String col:row.split(",")){
                    toRet.add(col);
                }                
            }
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return toRet;
    }
}
